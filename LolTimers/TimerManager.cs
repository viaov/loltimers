﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

using System.Configuration;

namespace LolTimers
{
    public class TimerManager
    {
        private static TimerManager _instance;
        public Dictionary<String, DateTime> timers = new Dictionary<String, DateTime>();

        public static TimerManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new TimerManager();
            }
            return _instance;
        }

        //start the timer
        private void StartTimer(string which) 
        {
            timers[which] = DateTime.Now.AddSeconds(int.Parse(ConfigurationSettings.AppSettings[which]));
        }
        //stop the timer
        private void StopTimer(string which)
        {
            timers[which] = DateTime.Now;
        }
        //toggle a given timer
        public bool ToggleTimer(string which)
        {
            //check if the timer is already running
            if (timers[which] > DateTime.Now)
            {
                //timer is active stop it and return true
                StopTimer(which);
                return false;
            }
            else
            {
                //timer is not running start it and return true
                StartTimer(which);
                return true;
            }
        }

        private TimerManager()
        {
            timers["TealBlue"] = DateTime.Now;
            timers["TealRed"] = DateTime.Now;
            timers["PurpleBlue"] = DateTime.Now;
            timers["PurpleRed"] = DateTime.Now;
            timers["Dragon"] = DateTime.Now;
            timers["Baron"] = DateTime.Now;
            timers["LeftSightWard"] = DateTime.Now;
            timers["LeftVisionWard"] = DateTime.Now;
            timers["RightSightWard"] = DateTime.Now;
            timers["RightVisionWard"] = DateTime.Now;
        }
    }
}
