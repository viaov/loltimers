﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Threading;

using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.IO;
using System.Configuration;

namespace LolTimers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary<string, Label> labels;
        public DispatcherTimer updater;
        public string mode;

        public MainWindow()
        {
            InitializeComponent();

            SetPositionFromDisk();

            mode = ConfigurationSettings.AppSettings["DisplayMode"];

            this.Closing += (object sender, System.ComponentModel.CancelEventArgs e) =>
            {
                WritePositionToDisk();
            };

            ContainerGrid.MouseDown += (object sender, MouseButtonEventArgs e) =>
            {
                if (Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    DragMove();
                    e.Handled = true;
                }
            };

            updater = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 100), DispatcherPriority.Normal, Tick, Dispatcher.CurrentDispatcher);

            labels = new Dictionary<string, Label>()
            {
                { "TealBlue", teal_blue_label },
                { "TealRed", teal_red_label },
                { "PurpleBlue", purple_blue_label },
                { "PurpleRed", purple_red_label },
                { "Dragon", dragon_label },
                { "Baron", baron_label },
                { "LeftSightWard", left_sight_ward_label },
                { "LeftVisionWard", left_vision_ward_label},
                { "RightSightWard", right_sight_ward_label },
                { "RightVisionWard", right_vision_ward_label },
            };

            updater.Start();
        }

        public void WritePositionToDisk()
        {
            try
            {
                if (!File.Exists("position.dat"))
                {
                    File.Create("position.dat").Close();
                }
                using (var writer = new StreamWriter(File.OpenWrite("position.dat")))
                {
                    writer.WriteLine(this.Top);
                    writer.WriteLine(this.Left);
                }
            }
            catch(Exception e)
            {
                //This isn't so bad if it fails anyways.
            }
        }

        public void SetPositionFromDisk()
        {
            this.Top = 0;
            this.Left = (System.Windows.SystemParameters.FullPrimaryScreenWidth / 2) - (this.Width / 2);

            try
            {
                if (File.Exists("position.dat"))
                {
                    using (var reader = new StreamReader(File.Open("position.dat", FileMode.Open)))
                    {
                        this.Top = double.Parse(reader.ReadLine());
                        this.Left = double.Parse(reader.ReadLine());
                    }
                }
            }
            catch
            {
                this.Top = 0;
                this.Left = (System.Windows.SystemParameters.FullPrimaryScreenWidth / 2) - (this.Width / 2);
            }
        }

        public void Tick(object sender, EventArgs e)
        {
            foreach (var l in labels)
            {
                if (TimerManager.GetInstance().timers[l.Key] <= DateTime.Now) //Timer Elapsed
                {
                    //Check if style is already set
                    if ((string)labels[l.Key].Content != "")
                    {
                        labels[l.Key].Content = "";
                        labels[l.Key].Background.Opacity = 0;
                    }
                }
                else
                {
                    var remaining = (TimerManager.GetInstance().timers[l.Key] - DateTime.Now);
                    string labelContent = mode == "minutes" ? 
                        String.Format("{0:D}:{1:D2}", remaining.Minutes, remaining.Seconds) :
                        String.Format("{0}", (int)((TimerManager.GetInstance().timers[l.Key] - DateTime.Now).TotalSeconds));
                    labels[l.Key].Content = labelContent;
                }
            }
        }

        public void ToggleTimer(string which)
        {
            TimerManager.GetInstance().ToggleTimer(which);
            labels[which].Background.Opacity = 79;
        }

        private void teal_blue_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("TealBlue");
            e.Handled = true;
        }

        private void teal_red_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("TealRed");
            e.Handled = true;
        }

        private void left_sight_ward_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("LeftSightWard");
            e.Handled = true;
        }

        private void left_vision_ward_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("LeftVisionWard");
            e.Handled = true;
        }

        private void baron_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("Baron");
            e.Handled = true;
        }

        private void dragon_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("Dragon");
            e.Handled = true;
        }

        private void right_sight_ward_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("RightSightWard");
            e.Handled = true;
        }

        private void right_vision_ward_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("RightVisionWard");
            e.Handled = true;
        }

        private void purple_red_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("PurpleRed");
            e.Handled = true;
        }

        private void purple_blue_button_Click(object sender, RoutedEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift) return;
            ToggleTimer("PurpleBlue");
            e.Handled = true;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            //Set the window style to noactivate.
            WindowInteropHelper helper = new WindowInteropHelper(this);
            SetWindowLong(helper.Handle, GWL_EXSTYLE,
                GetWindowLong(helper.Handle, GWL_EXSTYLE) | WS_EX_NOACTIVATE);
        }

        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_NOACTIVATE = 0x08000000;



        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowLong(IntPtr hWnd,

                                                  int nIndex,

                                                  int dwNewLong);

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd,

                                               int nIndex);
    }
}
